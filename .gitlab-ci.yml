include:
  - project: emfollow/gitlab-ci-templates
    file: lint.yml
  - project: computing/gitlab-ci-templates
    file: debian.yml
  - project: computing/gitlab-ci-templates
    file: python.yml


stages:
  - build
  - test
  - deploy

.install-cython: &install-cython
  ${PYTHON} -m pip install cython


# Build Python packages
build:
  image: python:3.9
  stage: build
  script:
    - python setup.py sdist -d . bdist_wheel -d .
  artifacts:
    paths:
      - '*.whl'
      - '*.tar.gz'
    expire_in: 1 day

.test:
    extends: .python:pytest
    stage: test
    before_script:
        - pip install --upgrade pip
        - pip install .
        - pip install pytest-cov
    coverage: '/^TOTAL\s+.*\s+(\d+\.?\d*)%/'
    script:
        - python setup.py test --addopts="--cov --cov-report=html --cov-report=term --junitxml=junit.xml"
    artifacts:
      paths:
        - htmlcov/

test:python3.9:
  extends: .test
  image: python:3.9

test:python3.10:
  extends: .test
  allow_failure: true
  image: python:3.10

test:python3.11:
  extends: .test
  allow_failure: true
  image: python:3.11

# Generate documentation
doc:
  stage: test
  extends:
    - .debian:base
    - .python:sphinx
  # FIXME: temporarily set image to Python 3.9 because default `python` image
  # is now Python 3.10 which lacks wheels for many packages right now.
  image: python:3.9
  variables:
    SOURCEDIR: doc
    REQUIREMENTS: .[doc]
  before_script:
    - *install-cython
    - !reference [".debian:base", before_script]
    - !reference [".python:sphinx", before_script]
    - apt-get install -y -qq graphviz
    - pip install -r doc-requirements.txt
  needs: []

# Publish docs on Git repository
pages:docs:
  stage: deploy
  needs:
    - doc
  only:
    - master
  script:
    - mv html public
  artifacts:
    paths:
      - public

# Publish coverage
pages:cov:
  stage: deploy
  needs:
    - test:python3.9
  script:
    - mv htmlcov public
  only:
    - master
  artifacts:
    paths:
      - public

FAR_study:
    stage: test
    when: manual
    image: python:3.9
    before_script:
        - pip install --upgrade pip
        - pip install .
    script:
        - python ligo/raven/tests/FAR_study.py

FAR_subgrb_study:
    stage: test
    when: manual
    image: python:3.9
    before_script:
        - pip install --upgrade pip
        - pip install .
    script:
        - python ligo/raven/tests/FAR_subgrb_study.py

lint:
  stage: test
  variables:
    GIT_STRATEGY: none
  before_script:
    - tar --strip-components 1 -xf *.tar.*
  dependencies:
    - build

release:
  # update readthedocs page
  stage: deploy
  image: python
  variables:
    GIT_STRATEGY: fetch
  # only update if new tag
  script:
    - |
      if [ -n "$READTHEDOCS_API_KEY" ]
      then 
         curl -X POST -d "branches=master" \
         -d "token=$READTHEDOCS_API_KEY" \
         "$READTHEDOCS_API_URL"
      fi
