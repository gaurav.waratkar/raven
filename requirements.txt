numpy>=1.14.5
healpy!=1.12.0  # FIXME: https://github.com/healpy/healpy/pull/457
gracedb-sdk
ligo-gracedb>=2.2.0
matplotlib
astropy
astropy-healpix
scipy>=0.7.2
ligo.skymap>=0.1.1
